## Demo: https://organization-portal.vercel.app/
## Table of Contents
  - [Overview](#overview)
  - [Tools](#tools)
  - [Installation](#installation)
  - [Testing](#testing)
    * [Unit Testing using Jest](#unit-testing-using-jest)
    * [End to End Testing using Cypress](#end-to-end-testing-using-cypress)
  - [Building](#building)

  ## Overview
  A company that uses an internal system to monitor the performance of its sales team. They are currently use [Vuejs](https://v2.vuejs.org/) and [Angularjs](https://docs.angularjs.org/guide/introduction)

  ![filter highlight functionality](filter.gif)

  The application consists of two pages
  - On the home page, there is a table that names all members of the sales team.

  - the performance-chart page, which provides a simple chart showing the team's performance during the previous week.

  ## Tools
  - [Angularjs](https://docs.angularjs.org/guide/introduction)
  - [Vuejs](https://v2.vuejs.org/)
  - [Vuex](https://vuex.vuejs.org/)
  - [Sass](https://sass-lang.com/)
  - [Cypress](https://www.cypress.io/)
  - [Jest](https://jestjs.io/)
  - [Webpack](https://webpack.js.org/)

  ## Installation
  - clone the repository
  - run `yarn install`
  - run `yarn start`

  ## Testing
  ### Unit Testing using Jest
  ```
  yarn run test:unit
  ```
  ### End to End Testing using Cypress
  ```
  yarn run test:e2e:ci
  ```
  To run the GUI which is usually perferable when developing
  ```
  yarn run test:e2e
  ```
  ## Building
  ```
  yarn build
  ```

