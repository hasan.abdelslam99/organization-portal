const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Development',
      template: 'app/index.html',
      filename: 'index.html',
    }),
    new ESLintPlugin({
      failOnError: true,
      emitWarning: false,
      outputReport: true,
    }),
    new VueLoaderPlugin(),
  ],
  devServer: {
    static: './dist',
    historyApiFallback: true,
    hot: true,
  },
});
