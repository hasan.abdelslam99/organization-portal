/* eslint-disable no-undef */

describe('home page', () => {
  beforeEach(() => {
    cy.fixture('employees').as('data');
    cy.intercept('/employees', {
      fixture: 'employees.json',
    }).as('employeesRequest');
  });
  it('should correctly show list of employees and links at the top to chart page', function () {
    cy.visit('/');
    cy.wait('@employeesRequest');

    cy.get('.c-users-list__body').should(
      'have.length',
      this.data.employees.length
    );

    cy.get('.top-links a[ui-sref="team-performance"]')
      .should('have.text', 'Team performance chart page')
      .should('have.attr', 'href', '/team-performance');
  });

  it('should correctly request next page when clicking load more', function () {
    cy.visit('/');
    cy.wait('@employeesRequest');

    cy.get('.c-users-list__body').should(
      'have.length',
      this.data.employees.length
    );

    const expectedQuery = {
      page: String(this.data.current_page + 1),
    };

    cy.intercept(
      '/employees*',
      {
        query: expectedQuery,
      },
      {
        fixture: 'employees-next-page',
      }
    ).as('employeesLoadMoreRequest');

    cy.get('.c-dashboard__load-more').click();
    cy.wait('@employeesLoadMoreRequest').then(({ response }) => {
      cy.get('.c-users-list__body').should(
        'have.length',
        this.data.employees.length + response.body.employees.length
      );
    });
  });
});
