angular.module('appModule').component('employeesList', {
  templateUrl: 'components/employees-list/employees-list.html',
  controller: EmployeesListComponent,
  controllerAs: 'EmployeesListComponentVm',
  bindings: {
    employeesList: '<',
    query: '<',
  },
});

angular.module('appModule').filter('highlight', function ($sce) {
  return function (input, q) {
    if (!q) return $sce.trustAsHtml(input);

    const pattern = new RegExp(q, 'gi');
    const filteredText = input.replace(
      pattern,
      (match) => `<span class="highlighted">${match}</span>`
    );
    return $sce.trustAsHtml(filteredText);
  };
});

function EmployeesListComponent() {}
