angular.module('appModule').controller('homeController', homePageController);

function homePageController(Employees, $location) {
  const homePageVm = this;
  homePageVm.employees = [];
  homePageVm.query = {
    filter: $location.search().filter || '',
    page: 1,
    pagesCount: 1,
  };
  homePageVm.loading = false;
  homePageVm.showLoadMore = true;

  activate();

  function activate() {
    homePageVm.loading = true;
    Employees.getEmployees().then(({ data }) => {
      homePageVm.employees = homePageVm.employees.concat(data.employees);
      homePageVm.query.pagesCount = data.pages;
      homePageVm.loading = false;
    });
  }

  // handlers

  homePageVm.handleChangeFilter = (q) => {
    homePageVm.query.filter = q;
    if (!q || !q.length) {
      $location.search('filter', null);
      return;
    }
    $location.search('filter', q);
  };

  homePageVm.handleLoadMoreEmployees = () => {
    homePageVm.loading = true;
    homePageVm.query.page += 1;
    Employees.loadMoreEmployees(homePageVm.query.page).then(({ data }) => {
      homePageVm.employees = homePageVm.employees.concat(data.employees);
      homePageVm.loading = false;
      if (homePageVm.query.page >= homePageVm.query.pagesCount) {
        homePageVm.showLoadMore = false;
      }
    });
  };
}
