import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';
import moment from 'moment';

Vue.use(Vuex);

const store = {
  state: {
    chartData: [],
    rawChartData: [],
  },
  mutations: {
    setChart(state, data) {
      state.chartData = data;
      state.rawChartData = data;
    },
    filterChart(state, q) {
      let { startDate, endDate } = q;

      startDate = moment(startDate, 'YYYY-MM-DD');
      endDate = moment(endDate, 'YYYY-MM-DD');

      state.chartData = state.rawChartData.filter((item) => {
        const { date_ms: date } = item;
        return moment(date).isBetween(startDate, endDate, 'day', '[]');
      });
    },
  },
  actions: {
    fetchChart({ commit }) {
      axios.get('https://fe-task.getsandbox.com/performance').then((res) => {
        commit('setChart', res.data);
      });
    },
  },
};

export default new Vuex.Store(store);
export const { mutations } = store;
