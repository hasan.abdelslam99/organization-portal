angular.module('appModule').service('Employees', EmployeesService);

function EmployeesService($http) {
  const getEmployees = () => {
    const employeesUrl = 'https://fe-task.getsandbox.com/employees';

    return $http.get(employeesUrl);
  };

  const loadMoreEmployees = (page) => {
    const employeesUrl = 'https://fe-task.getsandbox.com/employees';

    return $http.get(`${employeesUrl}?page=${page}`);
  };

  return {
    getEmployees,
    loadMoreEmployees,
  };
}
