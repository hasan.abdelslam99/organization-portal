import { mutations } from '../store';

describe('Date Range Filter', () => {
  it('should filter out data before start date and after end date', () => {
    const rawChartData = [
      {
        date_ms: 1641945600000,
        performance: 0.83,
      },
      {
        date_ms: 1642032000000,
        performance: 0.31,
      },
      {
        date_ms: 1642118400000,
        performance: 0.65,
      },
      {
        date_ms: 1642204800000,
        performance: 0.88,
      },
    ];

    const expected = [
      {
        date_ms: 1642032000000,
        performance: 0.31,
      },
      {
        date_ms: 1642118400000,
        performance: 0.65,
      },
    ];

    const q = {
      startDate: '2022-01-13',
      endDate: '2022-01-14',
    };

    const state = { rawChartData };
    mutations.filterChart(state, q);
    expect(state.chartData).toStrictEqual(expected);
  });
});
